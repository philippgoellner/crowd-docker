#!/bin/bash

env 
if [ $RAM_MIN -gt 0 ] && [ $RAM_MAX -gt 0 ]; then
	sed -i "s|^JAVA_OPTS=\"-Xms[[:digit:]]*m\s*-Xmx[[:digit:]]*m|JAVA_OPTS=\"-Xms${RAM_MIN}m -Xmx${RAM_MAX}m|g" ${CROWD_INSTALL}/apache-tomcat/bin/setenv.sh
fi

# Fetch backup data over the network
if [ "${BACKUP_HOST}" != "false" ] && [ ! -f ${CROWD_HOME}/DONTSYNC ]; then
	chmod 600 ${BACKUP_KEY_FILE}
	rsync -arcz -vv -e "ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${BACKUP_KEY_FILE}" ${BACKUP_USER}@${BACKUP_HOST}:${BACKUP_PATH}/* ${CROWD_HOME}
	touch ${CROWD_HOME}/DONTSYNC
	rm ${BACKUP_KEY_FILE}
	echo "Synchronized backup data over the network from ${BACKUP_HOST}"
fi

# Enable remote debug
if [ "$CROWD_REMOTE_DEBUG" = "true" ]; then
	echo "set remote debugging port"
	JAVA_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,address=$DEBUG_PORT,server=y,suspend=n $JAVA_OPTS"
fi

# Configure server.xml if application is behind reverse proxy
if [ "$PROXY_NAME" != "false" ] && [ ! -f ${CROWD_INSTALL}/skipproxy.conf ]; then
	echo "manipulate proxy name in server.xml"
        xmlstarlet ed -i /Server/Service/Connector -t attr -n proxyName -v ${PROXY_NAME} ${CROWD_INSTALL}/apache-tomcat/conf/server.xml > /tmp/generated-server.xml
        mv /tmp/generated-server.xml ${CROWD_INSTALL}/apache-tomcat/conf/server.xml
	touch ${CROWD_INSTALL}/skipproxy.conf
fi

# Configure server.xml if application is behind SSL enabled reverse proxy
if [ "$HTTPS" = "true" ]; then
	echo "enable https"
        xmlstarlet ed -i /Server/Service/Connector -t attr -n proxyPort -v 443 ${CROWD_INSTALL}/apache-tomcat/conf/server.xml > /tmp/generated-server.xml
        mv /tmp/generated-server.xml ${CROWD_INSTALL}/apache-tomcat/conf/server.xml
        xmlstarlet ed -i /Server/Service/Connector -t attr -n scheme -v https ${CROWD_INSTALL}/apache-tomcat/conf/server.xml > /tmp/generated-server.xml
        mv /tmp/generated-server.xml ${CROWD_INSTALL}/apache-tomcat/conf/server.xml
fi

if [ "$IMPORTCERT" = "true" ]; then 
        JAVA_OPTS="-Djavax.net.ssl.trustStore=/opt/jdk/current/jre/lib/security/cacerts -Djavax.net.ssl.trustStorePassword=changeit $JAVA_OPTS"      

	cd ${IMPORTPATH}
	if [ ! -f ${CROWD_INSTALL}/skipcert.conf ]; then
		for i in *
		do 
			/usr/lib/jvm/java-8-oracle/jre/bin/keytool -keystore /usr/lib/jvm/java-8-oracle/jre/lib/security/cacerts -importcert -alias $i -file $i -storepass changeit -noprompt
		done
		touch ${CROWD_INSTALL}/skipcert.conf
	fi
	cd ${CROWD_INSTALL}
fi

# Install and configure New Relic
#TODO
#if [ "$NEWRELIC" = "true" ]; then
#	unzip newrelic-java-${NEWRELIC_VERSION}.zip -d ${CROWD_INSTALL}
#
#	cd ${CROWD_INSTALL}/newrelic
#	java -jar newrelic.jar install
#	sed -ri "s/app_name:.*$/app_name: ${NEWRELIC_APP_NAME}/" newrelic.yml
#	sed -ri "s/license_key:[ \t]*'[^']+'/license_key: '${NEWRELIC_LICENSE}'/" newrelic.yml
#fi

# Loop used to wait for a service to be ready which this application depends on
if [ "$WAIT" = "true" ]; then

	is_ready() {
    		eval "$WAIT_COMMAND"
	}

	# wait until is ready
	i=0
	while ! is_ready; do
    		i=`expr $i + 1`
    		if [ $i -ge ${WAIT_LOOPS} ]; then
        		echo "$(date) - still not ready, giving up"
        		exit 1
    		fi
    	echo "WAITING: $(date) - waiting to be ready for ${WAIT_COMMAND}"
    	sleep ${WAIT_SLEEP}
	done
fi

. ${CROWD_INSTALL}/common.bash

#sudo own-volume
cd ${CROWD_INSTALL}/apache-tomcat/conf/Catalina/localhost
for k in $(ls) ; do
  unlink $k
done

if [ -n "$DEMO_CONTEXT" ]; then
  echo "Installing demo at $DEMO_CONTEXT"
  ln -s ${CROWD_INSTALL}/webapps/demo.xml ${DEMO_CONTEXT}.xml
fi

if [ -n "$SPLASH_CONTEXT" ]; then
  echo "Installing splash as $SPLASH_CONTEXT"
  ln -s ${CROWD_INSTALL}/webapps/splash.xml ${SPLASH_CONTEXT}.xml
fi

if [ -n "$OPENID_CLIENT_CONTEXT" ]; then
  echo "Installing OpenID client at $OPENID_CLIENT_CONTEXT"
  ln -s ${CROWD_INSTALL}/webapps/openidclient.xml ${OPENID_CLIENT_CONTEXT}.xml
fi

if [ -n "$CROWDID_CONTEXT" ]; then
  echo "Installing OpenID server at $CROWDID_CONTEXT"
  ln -s ${CROWD_INSTALL}/webapps/openidserver.xml ${CROWDID_CONTEXT}.xml
fi

if [ -n "$CROWD_CONTEXT" ]; then
  echo "Installing Crowd at $CROWD_CONTEXT"
  ln -s ${CROWD_INSTALL}/webapps/crowd.xml ${CROWD_CONTEXT}.xml
fi
cd ${CROWD_INSTALL}

if [ -z "$DEMO_LOGIN_URL" ]; then
  if [ "$DEMO_CONTEXT" == "ROOT" ]; then
    DEMO_LOGIN_URL="$LOGIN_BASE_URL/"
  else
    DEMO_LOGIN_URL="$LOGIN_BASE_URL/$DEMO_CONTEXT"
  fi
fi

if [ -z "$CROWDID_LOGIN_URL" ]; then
  if [ "$CROWDID_CONTEXT" == "ROOT" ]; then
    CROWDID_LOGIN_URL="$LOGIN_BASE_URL/"
  else
    CROWDID_LOGIN_URL="$LOGIN_BASE_URL/$CROWDID_CONTEXT"
  fi
fi

config_line() {
    local key="$(echo $2 | sed -e 's/[]\/()$*.^|[]/\\&/g')"
    if [ -n "$3" ]; then
      local value="$(echo $3 | sed -e 's/[\/&]/\\&/g')"
      sed -i -e "s/^$key\s*=\s*.*/$key=$value/" $1
    else
      sed -n -e "s/^$key\s*=\s*//p" $1
    fi
}

if [ -n "$CROWD_CONTEXT" ]; then
  if [ -z "$CROWDDB_URL" -a -n "$DATABASE_URL" ]; then
    used_database_url=1
    CROWDDB_URL="$DATABASE_URL"
  fi
  if [ -n "$CROWDDB_URL" ]; then
    extract_database_url "$CROWDDB_URL" CROWDDB ${CROWD_INSTALL}/apache-tomcat/lib
    CROWDDB_JDBC_URL="$(xmlstarlet esc "$CROWDDB_JDBC_URL")"
    cat << EOF > webapps/crowd.xml
    <Context docBase="../../crowd-webapp" useHttpOnly="true">
      <Resource name="jdbc/CrowdDS" auth="Container" type="javax.sql.DataSource"
                username="$CROWDDB_USER"
                password="$CROWDDB_PASSWORD"
                driverClassName="$CROWDDB_JDBC_DRIVER"
                url="$CROWDDB_JDBC_URL"
              />
    </Context>
EOF
  fi
fi

if [ -n "$CROWDID_CONTEXT" ]; then
  if [ -z "$CROWDIDDB_URL" -a -n "$DATABASE_URL" ]; then
    if [ -n "$used_database_url" ]; then
      echo "DATABASE_URL is ambiguous since both Crowd and CrowdID are enabled."
      echo "Please use CROWDIDDB_URL and CROWDDB_URL instead."
      exit 2
    fi
    CROWDIDDB_URL="$DATABASE_URL"
  fi
  if [ -n "$CROWDIDDB_URL" ]; then
    extract_database_url "$CROWDIDDB_URL" CROWDIDDB "${CROWD_INSTALL}/apache-tomcat/lib"
    CROWDIDDB_JDBC_URL="$(xmlstarlet esc "$CROWDIDDB_JDBC_URL")"
    cat << EOF > webapps/openidserver.xml
    <Context docBase="../../crowd-openidserver-webapp">
      <Resource name="jdbc/CrowdIDDS" auth="Container" type="javax.sql.DataSource"
                username="$CROWDIDDB_USER"
                password="$CROWDIDDB_PASSWORD"
                driverClassName="$CROWDIDDB_JDBC_DRIVER"
                url="$CROWDIDDB_JDBC_URL"
              />
    </Context>
EOF
    config_line build.properties hibernate.dialect "$CROWDIDDB_DIALECT"
  fi
fi

config_line build.properties demo.url "$DEMO_LOGIN_URL"
config_line build.properties openidserver.url "$CROWDID_LOGIN_URL"
config_line build.properties crowd.url "$CROWD_URL"

./build.sh

if [ -f "${CROWD_HOME}/crowd.properties" ]; then
	if [ ! -f ${CROWD_INSTALL}/skipcert.conf ]; then
# Write fixed localhost URL in crowd.properties for client libraries.
# Otherwise we will be locked out of Crowd.
  config_line ${CROWD_HOME}/crowd.properties crowd.server.url "$(config_line crowd-webapp/WEB-INF/classes/crowd.properties crowd.server.url)"
  config_line ${CROWD_HOME}/crowd.properties application.login.url "$(config_line crowd-webapp/WEB-INF/classes/crowd.properties application.login.url)"
	fi
fi

${CROWD_INSTALL}/start_crowd.sh -fg
